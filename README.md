# Old School WoW Talent Calculator

CSS-grid based talent calculator  
Usable talent calculator hosted here: https://jaywalker15262.github.io/oswow-talent-calc/
## Features

- URL encoded app state (for shareable talent builds)
- Code splitting per class
- Viewport-aware tooltip
- Authentic, raster-based game assets used for UI

## Wishlist / todo  

- Have values scale based on the "Required level" for spent talent points.  
- Better tooltip positioning for mobile layout.
